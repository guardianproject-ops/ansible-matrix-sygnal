#!/usr/bin/env python3
"""

sygnal-key-fetcher.py: script that fetches sygnal ios and android APNS/GCM configs from AWS SSM

(C) 2019-2020 Abel Luck <abel@guardianproject.info>

The Guardian Project
"""
from __future__ import print_function
import boto3
import json
import sys
import os


def is_debug():
    try:
        return "development" == os.environ["TAG_Environment"]
    except Exception:
        return False


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def getchildren(param_name, secure=True, region=os.environ.get("AWS_REGION")):
    """
    This function reads a secure parameter from AWS' SSM service.
    The request must be passed a valid parameter name, as well as
    temporary credentials which can be used to access the parameter.
    The parameter's value is returned.
    """
    # Create the SSM Client
    ssm = boto3.client("ssm", region_name=region)

    # Get the requested parameter
    response = ssm.get_parameters_by_path(
        Path=param_name, Recursive=True, WithDecryption=secure
    )

    return response["Parameters"]


def getparam(param_name, secure=True, region=os.environ.get("AWS_REGION")):
    """
    This function reads a secure parameter from AWS' SSM service.
    The request must be passed a valid parameter name, as well as
    temporary credentials which can be used to access the parameter.
    The parameter's value is returned.
    """
    ssm = boto3.client("ssm", region_name=region)

    response = ssm.get_parameters(Names=[param_name,], WithDecryption=secure)

    return response["Parameters"][0]["Value"]


def main():
    is_ci_skip = os.environ.get("CI_SKIP", None)
    if is_ci_skip:
        print("sygnal-key-fetcher: CI_SKIP defined, skipping run")
        sys.exit(0)
    namespace = os.environ["TAG_Namespace"]
    env = os.environ["TAG_Environment"]
    project = os.environ["TAG_Project"]
    name = os.environ["TAG_Name"]
    prefix = "/{}".format(name)
    key = "{}/apps/ios".format(prefix)
    output_path = "/etc/sygnal"
    apps = getchildren(key)
    if is_debug():
        print("sygnal-key-fetcher fetching {}".format(key))
    for param in apps:
        if is_debug():
            print("sygnal-key-fetcher processing {}".format(param["Name"]))
        app = json.loads(param["Value"])
        try:
            package = app["package"]
            if is_debug():
                print("sygnal-key-fetcher processing {}".format(package))
            key = app["key"]
            dest = "{}/{}.pem".format(output_path, package)
            if is_debug():
                print("sygnal-key-fetcher writing {}".format(dest))

            with open(dest, "w") as f:
                f.write(key)
        except Exception as e:
            eprint(
                (
                    "sygnal-key-fetcher error processing path "
                    "'{}'".format(param["Name"])
                )
            )
            print(e)
            sys.exit(2)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        eprint("sygnal-key-fetcher: error fetching keys from AWS SSM")
        print(e)
        sys.exit(1)
