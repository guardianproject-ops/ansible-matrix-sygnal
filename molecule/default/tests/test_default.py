import os
import yaml
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directories(host):
    dirs = [
            "/var/lib/sygnal",
            "/var/log/sygnal",
            "/var/run/sygnal",
            "/usr/local/sygnal",
            "/etc/sygnal",
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory


def test_files(host):
    files = [
            "/etc/systemd/system/matrix-sygnal.service",
            "/etc/logrotate.d/sygnal",
            "/etc/sygnal/sygnal.yml",
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

    f = host.file("/usr/local/bin/sygnal-key-fetcher")
    assert f.exists
    assert f.mode == 0o755


def test_service(host):
    s = host.service("matrix-sygnal")
    assert s.is_enabled
    assert s.is_running


def test_logrotate(host):
    cmd = host.run("logrotate -v /etc/logrotate.d/sygnal")
    print(cmd.stderr)
    assert cmd.rc == 0

def test_config_is_valid_yaml(host):
    f = host.file("/etc/sygnal/sygnal.yml")
    content = f.content_string
    doc = yaml.safe_load(content)
    foo = doc['apps']['example.foo']
    assert foo['type'] == 'apns'
    assert foo['certfile'] == '/etc/sygnal/example.foo.pem'


@pytest.mark.parametrize(
    "name,content",
    [
        ("/etc/ssl/openssl.cnf", "CipherString = DEFAULT@SECLEVEL=1"),
    ],
)
def test_file_content(host, name, content):
    f = host.file(name)
    assert f.exists
    if content:
        assert f.contains(content)
