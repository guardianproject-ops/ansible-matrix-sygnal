## Role Variables

* `sygnal_git_version`: `v0.8.2` - the sygnal git tag to install



* `sygnal_git_url`: `git://github.com/matrix-org/sygnal.git` - the git repo to use



* `sygnal_log_days_keep`: `30` - the number of days to keep sygnal logs on disk



* `sygnal_android_apps`: `` - a list of objects with keys 'package' and 'api_key'



* `sygnal_ios_apps`: `` - a list of objects with key 'package', and 'platform'



* `sygnal_postgres_enabled`: `false` - whether to enable postgres support or not



* `sygnal_db_hostname`: `` - postgres db hostname



* `sygnal_db_port`: `` - postgres db port



* `sygnal_db_name`: `` - db name



* `sygnal_db_username`: `` - db username



* `sygnal_db_password`: `` - db password



* `sygnal_db_cp_min`: `1` - min connection pool size



* `sygnal_db_cp_max`: `5` - max connection pool size


